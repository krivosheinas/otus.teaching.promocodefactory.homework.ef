﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)]
        public string Email { get; set; }
       
        public ICollection<Preference> Preferences { get; set; }

        public Customer()
        {
            Preferences = new List<Preference>();
        }
        
        //TODO: Списки Preferences и Promocodes 
    }
}