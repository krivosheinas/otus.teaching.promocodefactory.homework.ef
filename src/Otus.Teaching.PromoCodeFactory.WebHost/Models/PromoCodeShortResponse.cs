﻿using System;
using System.Globalization;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public Guid Id { get; set; }
        
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public string PartnerName { get; set; }

        public PromoCode MapToPromoCodeEntity()
        {
            return new PromoCode()
            {
                Id = this.Id,
                Code = this.Code,
                ServiceInfo = this.ServiceInfo,
                BeginDate = DateTime.ParseExact(this.BeginDate, "yyyy-MM-dd", new CultureInfo("ru-RU", true)),
                EndDate = DateTime.ParseExact(this.EndDate, "yyyy-MM-dd", new CultureInfo("ru-RU", true)),
                PartnerName = this.PartnerName
            };
        }

        public PromoCodeShortResponse Map(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd");
            EndDate = promoCode.EndDate.ToString("yyyy-MM-dd");
            PartnerName = promoCode.PartnerName;

            return this;
        }
    }
}