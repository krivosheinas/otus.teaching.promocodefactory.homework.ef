﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Repositories
{
    public class CustomerRepository: EFGenericRepository<Customer>
    {
        public CustomerRepository(DbContext context) : base(context)
        {
        }
        
    }
}