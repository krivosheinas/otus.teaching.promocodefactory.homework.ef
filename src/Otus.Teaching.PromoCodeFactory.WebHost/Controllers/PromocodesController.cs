﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>

    public class PromocodesController
        : BaseController
    {

        private readonly IGenericRepository<PromoCode> _promoRepository;

        public PromocodesController( IGenericRepository<PromoCode> promoRepository)
        {
            _promoRepository = promoRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoRepository.GetAllAsync();

            List<PromoCodeShortResponse> shortRespList = new List<PromoCodeShortResponse>();
            foreach(var promoCode in promoCodes)
            {
                shortRespList.Add(new PromoCodeShortResponse().Map(promoCode));

            }

            return shortRespList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            throw new NotImplementedException();
        }
    }
}