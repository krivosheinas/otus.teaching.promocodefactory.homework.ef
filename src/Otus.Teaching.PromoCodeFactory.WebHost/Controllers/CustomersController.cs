﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Namotion.Reflection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    public class CustomersController
        : BaseController
    {

        private readonly IGenericRepository<Customer> _customerRepository;
        private readonly IGenericRepository<Preference> _prefRepository;

        public CustomersController(IGenericRepository<Customer> customerRepository, IGenericRepository<Preference> prefRepository)
        {
            _customerRepository = customerRepository;
            _prefRepository = prefRepository;
        }        

        /// <summary>
        /// Получение данных по всем клиентам        
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
           
           List<CustomerShortResponse> shortResponces = new List<CustomerShortResponse>();
            foreach (var customer in customers)
            {
                shortResponces.Add(
                    new CustomerShortResponse()
                    {
                        Id = customer.Id,
                        Email = customer.Email,
                        FirstName = customer.FirstName,
                        LastName = customer.LastName
                    }
                );

            }
            return shortResponces;

        }
        

        /// <summary>
        /// Получение данных клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.FindByIdAsync(id);

            return new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }
        
        /// <summary>
        /// Добавление нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async  Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = _prefRepository.GetAll(x => request.PreferenceIds.Contains(x.Id)).ToList()
            };

            _customerRepository.Create(customer);
            return Created("/customers",customer);
        }

        /// <summary>
        /// Изменение данных по клиенту
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.FindByIdAsync(id);

            if (customer != null)
            {
                customer.FirstName = request.FirstName;
                customer.LastName = request.LastName;
                customer.Email = request.Email;                
                customer.Preferences = _prefRepository.GetAll(x=> request.PreferenceIds.Contains(x.Id)).ToList();
                _customerRepository.Update(customer);
                return Ok();
            }
            else
            {
                return BadRequest($"Клиент с id {id} не найден");
            }          
            return Ok();
        }
        
        /// <summary>
        /// Удаление клиента с каскадным удалением промокода
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.FindByIdAsync(id);
            if (customer != null)
            {
                _customerRepository.Remove(customer);
                return NoContent();

            }
            else
            {
                return BadRequest();
            }


        }
    }
}