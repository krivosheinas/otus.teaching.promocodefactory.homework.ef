﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{

    public class PreferencesController : BaseController
    {
        private readonly IGenericRepository<Preference> _prefRepository;

        public PreferencesController(IGenericRepository<Preference> prefRepository)
        {
            _prefRepository = prefRepository;
        }

        /// <summary>
        /// Возвращает список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Preference>>> GetPreferencesAsync()
        {
            return Ok(await _prefRepository.GetAllAsync());
        }

        /// <summary>
        /// Возвращает список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Preference>> GetPreferenceByIdAsync(Guid id)
        {
            return Ok(await _prefRepository.FindByIdAsync(id));
        }

    }
}
