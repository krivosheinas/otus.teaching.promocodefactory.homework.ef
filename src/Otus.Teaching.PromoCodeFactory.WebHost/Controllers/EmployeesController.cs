﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    public class EmployeesController
        : BaseController
    {
        private readonly IGenericRepository<Employee> _employeeRepository; 
        private readonly IGenericRepository<Role> _roleRepository; 
        public EmployeesController(IGenericRepository<Employee> employeeRepository,
                                   IGenericRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Employee>> GetEmployee()
        {
            var employees = await _employeeRepository.GetAllAsync();
            foreach (var employee in employees)
            {
                employee.Role = await _roleRepository.FindByIdAsync(employee.RoleId);
            }
            return employees;
        }
        
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Employee>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.FindByIdAsync(id);
            employee.Role = await _roleRepository.FindByIdAsync(employee.RoleId);
            return employee;
        }
    }
}