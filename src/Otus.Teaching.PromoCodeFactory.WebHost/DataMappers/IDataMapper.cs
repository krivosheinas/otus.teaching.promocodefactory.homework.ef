﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DataMappers
{
    public interface IDataMapper<R,M> where R: class where M: class
    {
        R Map(M data);
    }
}
