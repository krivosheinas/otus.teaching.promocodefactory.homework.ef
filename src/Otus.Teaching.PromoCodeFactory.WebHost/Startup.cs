using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        
        private IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped(typeof(IRepository<Employee>), (x) => 
                new InMemoryRepository<Employee>(FakeDataFactory.Employees));
            services.AddScoped(typeof(IRepository<Role>), (x) => 
                new InMemoryRepository<Role>(FakeDataFactory.Roles));
            services.AddScoped(typeof(IRepository<Preference>), (x) => 
                new InMemoryRepository<Preference>(FakeDataFactory.Preferences));
            services.AddScoped(typeof(IRepository<Customer>), (x) => 
                new InMemoryRepository<Customer>(FakeDataFactory.Customers));

            

            services.AddDbContext<PromocodeDbContext>(options => 
                options.UseSqlite(Configuration.GetConnectionString("sqlite")) 
                //options.UseNpgsql(Configuration.GetConnectionString("postgre"))
            );
            
            services.AddScoped(typeof(IGenericRepository<Role>), (x) => new EFGenericRepository<Role>(x.GetService<PromocodeDbContext>()));
            services.AddScoped(typeof(IGenericRepository<Employee>), (x) => new EFGenericRepository<Employee>(x.GetService<PromocodeDbContext>()));
            services.AddScoped(typeof(IGenericRepository<Customer>), (x) => new EFGenericRepository<Customer>(x.GetService<PromocodeDbContext>()));
            services.AddScoped(typeof(IGenericRepository<Preference>), (x) => new EFGenericRepository<Preference>(x.GetService<PromocodeDbContext>()));
            services.AddScoped(typeof(IGenericRepository<PromoCode>), (x) => new EFGenericRepository<PromoCode>(x.GetService<PromocodeDbContext>()));
            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {                       
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
                        
            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var scope = app.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetService<PromocodeDbContext>();
            InitDatabase(context);
        }


        public static void InitDatabase(PromocodeDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();                
            context.Employees.AddRange(FakeDataFactory.Employees);
            context.Roles.AddRange(FakeDataFactory.Roles);
            context.Customers.AddRange(FakeDataFactory.Customers);          
            context.PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            context.SaveChanges();                               
        }


        private static void DisplayStates(IEnumerable<EntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                Console.WriteLine($"Entity: {entry.Entity.GetType().Name},State: { entry.State.ToString()}");
            }
        }
    }
}