﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,              
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId =  Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,              
                AppliedPromocodesCount = 10
            },
            new Employee()
            {
                Id = Guid.Parse("5a6fae72-5fea-11ec-bf63-0242ac130002"),
                Email = "eliseev@somemail.ru",
                FirstName = "Владимир",
                LastName = "Елисеев",
                RoleId =  Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,               
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("20d639fd-3af7-42d3-b203-08e089aaa416"),
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                EmployeeId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PartnerName = Employees.SingleOrDefault(x => x.Id == Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895")).FullName,
                BeginDate = DateTime.Now.Date,
                EndDate = DateTime.Now.Date.AddDays(30),
                Code = "PROMO_THEATHRE",
                ServiceInfo ="ТЕАТРАЛЬНЫЙ АНШЛАГ",
                CustomerId =  Guid.Parse("2af10885-7912-432c-8bfa-7c61845e9c1e"),
            },
            new PromoCode()
            {
                Id = Guid.Parse("d66b6bce-5fea-11ec-bf63-0242ac130002"),
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                EmployeeId = Guid.Parse("5a6fae72-5fea-11ec-bf63-0242ac130002"),
                PartnerName = Employees.SingleOrDefault(x => x.Id == Guid.Parse("5a6fae72-5fea-11ec-bf63-0242ac130002")).FullName,
                BeginDate = DateTime.Now.Date.AddDays(-10),
                EndDate = DateTime.Now.Date.AddDays(10),
                Code = "PROMO_FAMILY",
                ServiceInfo = "СЕМЕЙНЫЙ СЕЗОН",
                CustomerId = Guid.Parse("894cc1e6-2bb9-45dc-b4d2-593ea8ad9387"),
            },
            new PromoCode()
            {
                Id = Guid.Parse("e0b4083e-5fea-11ec-bf63-0242ac130002"),
                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                EmployeeId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PartnerName = Employees.SingleOrDefault(x => x.Id == Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f")).FullName,
                BeginDate = DateTime.Now.Date.AddDays(-15),
                EndDate = DateTime.Now.Date.AddDays(20),
                Code = "PROMO_KIDS",
                ServiceInfo = "ДЕТСКАЯ МЕЧТА",
                CustomerId = Guid.Parse("56799c27-393d-4693-a695-7fc0542a6fe2"),
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = Guid.Parse("894cc1e6-2bb9-45dc-b4d2-593ea8ad9387"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",                    
                        Preferences = new List<Preference>{Preferences.FirstOrDefault(x=>x.Name=="Семья")}
                        //TODO: Добавить предзаполненный список предпочтений
                    },
                    
                    new Customer()
                    {
                        Id = Guid.Parse("2af10885-7912-432c-8bfa-7c61845e9c1e"),
                        Email = "andrey_ivanov@mail.ru",
                        FirstName = "Андрей",
                        LastName = "Иванов",                         
                        Preferences = new List<Preference>{Preferences.FirstOrDefault(x=>x.Name=="Театр")}
                        //TODO: Добавить предзаполненный список предпочтений
                    },
                    
                    new Customer()
                    {
                        Id = Guid.Parse("56799c27-393d-4693-a695-7fc0542a6fe2"),
                        Email = "marina_kruk@yandex.ru",
                        FirstName = "Марина",
                        LastName = "Крук",
                         Preferences = new List<Preference>{Preferences.FirstOrDefault(x=>x.Name=="Дети")}
                        //TODO: Добавить предзаполненный список предпочтений
                    }
                };

                return customers;
            }
        }
    }
}